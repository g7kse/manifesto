*Space is humanity’s future.*

It is humanity’s opportunity to explore, develop, use, and thrive differently. A way to ensure the *longevity, sustainability, openness, equality* of those efforts for all humanity. 

For this, we pledge to adhere to the following:

Principles
----

1. <span style="font-size:larger;">All people shall have the right to explore and use outer space for the benefit and in the interests of all humanity.</span>

2. <span style="font-size:larger;">Exploration and use of Outer Space shall be carried out collaboratively.</span>

3. <span style="font-size:larger;">Outer space shall be used exclusively for peaceful purposes.</span>

4. <span style="font-size:larger;">Profit should not be the driving force for space exploration.</span>

5. <span style="font-size:larger;">All people shall have access to Outer Space, space technologies, and space data.</span>

***

To achieve those principles, we need to adhere to the following:

Pillars
----

1.  **Open Source**

    All technologies developed for Outer Space shall be published and licensed using permissive open source licenses.

2.  **Open Data**

    All data related to and produced in Outer Space shall be freely used, shared, and built upon by anyone, anywhere, for any purpose.

3.  **Open Development**

    All technologies for Outer Space shall be developed in a transparent, legible, documented, testable, modular, and economical way.

4.  **Open Governance**

    All technologies for Outer Space shall be governed in a participatory, collaborative, direct, and distributed way.
